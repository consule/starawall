/*
SQLyog Ultimate v12.4.1 (64 bit)
MySQL - 5.6.41-84.1 : Database - consu943_strwall
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`consu943_strwall` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `consu943_strwall`;

/*Table structure for table `acesso` */

DROP TABLE IF EXISTS `acesso`;

CREATE TABLE `acesso` (
  `acessoId` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`acessoId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `acesso` */

insert  into `acesso`(`acessoId`,`descricao`) values 
(1,'Café Lamosic'),
(2,'Camarote Oba'),
(3,'Pista');

/*Table structure for table `funcaoPessoa` */

DROP TABLE IF EXISTS `funcaoPessoa`;

CREATE TABLE `funcaoPessoa` (
  `funcaoId` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`funcaoId`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `funcaoPessoa` */

insert  into `funcaoPessoa`(`funcaoId`,`descricao`) values 
(1,'Segurança'),
(2,'Faxineiro/a'),
(3,'Bar Man'),
(4,'Operações'),
(5,'Eletricista'),
(6,'Equipe de Som'),
(7,'STAFF');

/*Table structure for table `pessoa` */

DROP TABLE IF EXISTS `pessoa`;

CREATE TABLE `pessoa` (
  `pessoaId` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) CHARACTER SET latin1 NOT NULL,
  `cpf` varchar(20) CHARACTER SET latin1 NOT NULL,
  `nascimento` date DEFAULT NULL,
  `sexo` int(11) DEFAULT NULL,
  `foto` text,
  `criado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletado` int(11) NOT NULL DEFAULT '0',
  `dataDeletou` datetime DEFAULT NULL,
  `funcaoId` int(11) NOT NULL,
  `acessoId` int(11) DEFAULT NULL,
  `sexoId` int(11) NOT NULL,
  PRIMARY KEY (`pessoaId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `pessoa` */

insert  into `pessoa`(`pessoaId`,`nome`,`cpf`,`nascimento`,`sexo`,`foto`,`criado`,`deletado`,`dataDeletou`,`funcaoId`,`acessoId`,`sexoId`) values 
(1,'Cri Do Cavaco','999.999.999-99','2019-03-06',NULL,NULL,'2019-03-05 09:25:57',0,NULL,0,NULL,1),
(2,'Cri','555.555.555-55','2019-03-21',NULL,NULL,'2019-03-06 18:55:59',0,NULL,0,NULL,1),
(3,'Delza Pina','111.111.111-11','1983-05-22',NULL,NULL,'2019-03-06 20:15:07',1,'2019-03-07 07:14:35',0,NULL,2);

/*Table structure for table `sexo` */

DROP TABLE IF EXISTS `sexo`;

CREATE TABLE `sexo` (
  `sexoId` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sexoId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `sexo` */

insert  into `sexo`(`sexoId`,`descricao`) values 
(1,'Masculino'),
(2,'Feminino'),
(3,'Outros');

/*Table structure for table `tbl_items` */

DROP TABLE IF EXISTS `tbl_items`;

CREATE TABLE `tbl_items` (
  `itemId` int(11) NOT NULL AUTO_INCREMENT,
  `itemHeader` varchar(512) NOT NULL COMMENT 'Heading',
  `itemSub` varchar(1021) NOT NULL COMMENT 'sub heading',
  `itemDesc` text COMMENT 'content or description',
  `itemImage` varchar(80) DEFAULT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`itemId`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_items` */

insert  into `tbl_items`(`itemId`,`itemHeader`,`itemSub`,`itemDesc`,`itemImage`,`isDeleted`,`createdBy`,`createdDtm`,`updatedDtm`,`updatedBy`) values 
(1,'jquery.validation.js','Contribution towards jquery.validation.js','jquery.validation.js is the client side javascript validation library authored by Jörn Zaefferer hosted on github for us and we are trying to contribute to it. Working on localization now','validation.png',0,1,'2015-09-02 00:00:00',NULL,NULL),
(2,'CodeIgniter User Management','Demo for user management system','This the demo of User Management System (Admin Panel) using CodeIgniter PHP MVC Framework and AdminLTE bootstrap theme. You can download the code from the repository or forked it to contribute. Usage and installation instructions are provided in ReadMe.MD','cias.png',0,1,'2015-09-02 00:00:00',NULL,NULL);

/*Table structure for table `tbl_reset_password` */

DROP TABLE IF EXISTS `tbl_reset_password`;

CREATE TABLE `tbl_reset_password` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `activation_id` varchar(32) NOT NULL,
  `agent` varchar(512) NOT NULL,
  `client_ip` varchar(32) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdBy` bigint(20) NOT NULL DEFAULT '1',
  `createdDtm` datetime NOT NULL,
  `updatedBy` bigint(20) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_reset_password` */

insert  into `tbl_reset_password`(`id`,`email`,`activation_id`,`agent`,`client_ip`,`isDeleted`,`createdBy`,`createdDtm`,`updatedBy`,`updatedDtm`) values 
(22,'cristianoconsule@gmail.com','HrhfKBykCvgZaUz','Chrome 72.0.3626.109','189.63.228.25',0,1,'2019-02-19 17:20:09',NULL,NULL),
(23,'cristianoconsule@gmail.com','pTuWr1jLHea9gOC','Chrome 72.0.3626.109','189.56.95.90',0,1,'2019-02-22 13:19:13',NULL,NULL),
(24,'cristianoconsule@gmail.com','ufw5jtLpKCObyzo','Chrome 72.0.3626.119','179.111.246.200',0,1,'2019-02-26 17:21:27',NULL,NULL),
(25,'cristianoconsule@gmail.com','Uut07BDmQpdvIyh','Chrome 72.0.3626.119','177.82.85.1',0,1,'2019-02-28 21:42:31',NULL,NULL),
(26,'cristianoconsule@gmail.com','vLQt05gqOHWP4BG','Chrome 72.0.3626.119','177.82.85.1',0,1,'2019-02-28 21:43:48',NULL,NULL),
(27,'cristianoconsule@gmail.com','Out2wVM0lZSBD8G','Chrome 72.0.3626.119','177.82.85.1',0,1,'2019-02-28 21:48:38',NULL,NULL),
(28,'cristianoconsule@gmail.com','Qaty5SChXgETJi7','Chrome 72.0.3626.119','177.82.85.1',0,1,'2019-02-28 22:02:31',NULL,NULL);

/*Table structure for table `tbl_roles` */

DROP TABLE IF EXISTS `tbl_roles`;

CREATE TABLE `tbl_roles` (
  `roleId` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'role id',
  `role` varchar(50) NOT NULL COMMENT 'role text',
  PRIMARY KEY (`roleId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_roles` */

insert  into `tbl_roles`(`roleId`,`role`) values 
(1,'Administrador do Sistema'),
(2,'Gerente'),
(3,'Operador/Empregado');

/*Table structure for table `tbl_users` */

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `roleId` tinyint(4) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_users` */

insert  into `tbl_users`(`userId`,`email`,`password`,`name`,`mobile`,`roleId`,`isDeleted`,`createdBy`,`createdDtm`,`updatedBy`,`updatedDtm`) values 
(1,'cristianoconsule@gmail.com','$2y$10$SpVcxEqb1aO6.Qyu9i1pmetBNKdQqgz47NdjkxVCCfsngxrS3BAB.','Administrador do Sistema','9890098900',1,0,0,'2015-07-01 18:56:49',1,'2019-03-04 08:35:18'),
(2,'cristiano@consule.com.br','$2y$10$Gkl9ILEdGNoTIV9w/xpf3.mSKs0LB1jkvvPKK7K0PSYDsQY7GE9JK','Gerente','1699106138',2,1,1,'2016-12-09 17:49:56',1,'2019-02-20 17:10:26'),
(3,'consule@consule.com.br','$2y$10$MB5NIu8i28XtMCnuExyFB.Ao1OXSteNpCiZSiaMSRPQx1F1WLRId2','Empregado Teste','9890098900',3,1,1,'2016-12-09 17:50:22',1,'2019-02-20 17:10:52'),
(4,'cristiano@consule.com.br','$2y$10$07r8wahBcky7cGqhxT5Y6e9JFI1ymUrs.FdOJLesOGlMFuID68H7m','Gerente','9999999999',3,1,1,'2019-02-19 17:23:16',1,'2019-03-04 23:35:40'),
(5,'renata@loira.sex','$2y$10$slqc/8C1SfP4E7qucQYN5uxz1XfmSupm3fKm/VaYjKyZApoRtTyLK','Renata','9999999999',2,1,1,'2019-02-21 18:43:54',1,'2019-02-21 19:33:40'),
(6,'admin@bewithdhanu.in','$2y$10$t8oxMlzab7mYhP0FNZBoVuD/A2bnn.vTmrP0O/P4vGnUI8WP6mZqq','Teste','2222222222',2,1,1,'2019-02-21 19:35:07',1,'2019-02-21 19:35:49'),
(7,'fabricio@choupina.com','$2y$10$S1cEZnMsUCkrUiMRmlFYFem35dduZbFTnqggRBtWhfT9FrtLVdNCq','Fabrício Choupina','1111111111',2,1,1,'2019-02-27 09:45:56',1,'2019-03-04 23:35:37'),
(8,'obafestival@oba.com','$2y$10$Af6vblyXTU9Nj6V4dssJ9eMG6m4Am9us5yV2ibc0kC1F41AkwzTQe','Obafestival','1111111111',3,1,1,'2019-02-27 09:46:52',1,'2019-03-04 23:35:43');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
