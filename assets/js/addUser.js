/**
 * File : addUser.js
 * 
 * This file contain the validation of add user form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */

$(document).ready(function(){
	
	var addUserForm = $("#addUser");
	
	var validator = addUserForm.validate({
		
		rules:{
			nome :{ required : true },
			cpf :{ required : true },
			email : { required : true, email : true, remote : { url : baseURL + "checkEmailExists", type :"post"} },
			password : { required : true },
			cpassword : {required : true, equalTo: "#password"},
			mobile : { required : true, digits : true },
			role : { required : true, selected : true}
		},
		messages:{
			nome :{ required : "Este campo é obrigatório!" },
			cpf :{ required : "Este campo é obrigatório!" },
			email : { required : "Este campo é obrigatório!", email : "Please enter valid email address", remote : "Email already taken" },
			password : { required : "Este campo é obrigatório!" },
			cpassword : {required : "Este campo é obrigatório!", equalTo: "Please enter same password" },
			mobile : { required : "Este campo é obrigatório!", digits : "Please enter numbers only" },
			role : { required : "Este campo é obrigatório!", selected : "Selecione uma opção" }			
		}
	});
});
