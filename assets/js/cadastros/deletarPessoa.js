/**
 * @author Kishor Mali
 */


jQuery(document).ready(function(){
	
	jQuery(document).on("click", ".deletarPessoa", function(){
		var pessoaId = $(this).data("userid"),
			hitURL = baseURL + "cadastros/deletarPessoa",
			currentRow = $(this);

        var confirmation = confirm("Tem certeza que deseja excluir?");
        alert(hitURL);
		if(confirmation) 
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { 
                pessoaId : pessoaId 
            }  
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) {
                    alert("Excluido com sucesso"); 
                } else if(data.status = false) { 
                    alert("User deletion failed"); 
                } else { 
                    alert("Access denied..!"); 
                }
			});
		}
	});
	
	
	jQuery(document).on("click", ".searchList", function(){
		
	});
	
});
