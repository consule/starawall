<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Pessoa extends BaseController{

    public function __construct() {

        parent::__construct();
        $this->load->model('cadastros/pessoa_model');
        $this->isLoggedIn(); 

    }

    public function index()
    {
        $this->global['pageTitle'] = 'CodeInsect : Dashboard';
        $this->load->model('cadastros/pessoas_model');
        $this->loadViews("cadastros/pessoas", $this->global, NULL , NULL);
    }

    function listaPessoa() {

        $this->load->model('cadastros/pessoa_model');
    
        $searchText = $this->input->post('searchText');

        $data['searchText'] = $searchText;
        
        $this->load->library('pagination');
        
        $count = $this->pessoa_model->contaPessoas($searchText);

        $returns = $this->paginationCompress ( "listaPessoa/", $count, 10 );
        
        $data['usuariosGravados'] = $this->pessoa_model->listaPessoa($searchText, $returns["page"], $returns["segment"]);
        
        $this->global['pageTitle'] = 'Pessoas : Lista de Pessoas';
        
        $this->loadViews("cadastros/pessoas", $this->global, $data, NULL);
    }

    function adicionarNova() {
       
        $this->load->model('cadastros/pessoa_model');

        $data['sexo'] = $this->pessoa_model->getSexos();
        
        $this->global['pageTitle'] = 'Pessoa : Adicionar Nova Pessoa';

        //A Variável $data carrega as categorias, caso tenha no form
        $this->loadViews("cadastros/adicionarNova", $this->global, $data, NULL);

        
    }

    function adicionarNovaPessoa() {
       
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('nome','Nome Completo','trim|required|max_length[128]|xss_clean');
        $this->form_validation->set_rules('cpf', 'CPF', 'trim|required|max_length[100]|xss_clean');
        $this->form_validation->set_rules('nascimento','Nascimento','trim|required');
        $this->form_validation->set_rules('sexoId','Sexo','trim|required|numeric');
        
        if($this->form_validation->run() == FALSE) {
            $this->adicionarNova();
        } else {
            $nome = ucwords(strtolower($this->input->post('nome')));
            $cpf = $this->input->post('cpf');
            $nascimento = $this->input->post('nascimento');
            $sexoId = $this->input->post('sexoId');
            
            $userInfo = array('nome'=>$nome, 'cpf'=>$cpf, 'nascimento'=>$nascimento, 'sexoId'=>$sexoId);
            
            $this->load->model('cadastros/pessoa_model');
            
            $result = $this->pessoa_model->adicionarNovaPessoa($userInfo);
            
            if($result > 0) {
                $this->session->set_flashdata('success', 'Nova pessoa Inserida com Sucesso!!!');
            } else {
                $this->session->set_flashdata('error', 'Criação da Pessoa Falhou!');
            } 

            redirect('cadastros/adicionarNova');
        }
    }


    function editarAntigo($pessoaId = NULL) {
       
        $data['usuariosGravados'] = $this->pessoa_model->getUserInfo($pessoaId);

        // $data['roles'] = $this->pessoa_model->getPessoaRoles();

        $data['sexo'] = $this->pessoa_model->getSexos();

        $this->global['pageTitle'] = 'Pessoa : Editar Pessoa';
        
        $this->loadViews("cadastros/editarAntigo", $this->global, $data, NULL);

    }

    function editarPessoa() {

        $this->load->library('form_validation');
        
        $pessoaId = $this->input->post('pessoaId'); 
        
        $this->form_validation->set_rules('nome','Nome Completo','trim|required|max_length[128]|xss_clean');
        $this->form_validation->set_rules('cpf', 'CPF', 'trim|required|max_length[100]|xss_clean');
        $this->form_validation->set_rules('nascimento','Nascimento','trim|required');
        $this->form_validation->set_rules('sexoId','Sexo','trim|required|numeric');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->editarAntigo($pessoaId);
        }
        else
        {
            $nome = ucwords(strtolower($this->input->post('nome')));
            $cpf = $this->input->post('cpf');
            $nascimento = $this->input->post('nascimento');
            $sexoId = $this->input->post('sexoId');
            
            $userInfo = array('nome'=>$nome, 'cpf'=>$cpf, 'nascimento'=>$nascimento, 'sexoId'=>$sexoId);
            
            $this->load->model('cadastros/pessoa_model');
      
            $result = $this->pessoa_model->editarPessoa($userInfo, $pessoaId);
            
            if($result == true)
            {
                $this->session->set_flashdata('success', 'Pessoa Atualizada com Sucesso');
            }
            else
            {
                $this->session->set_flashdata('error', 'User updation failed');
            }
            
            redirect('cadastros/editarAntigo/'.$pessoaId);
        }
    }

    function deletarPessoa() {

        if($this->isAdmin() == TRUE) {
            echo(json_encode(array('status'=>'access')));
        } else {
            $userId = $this->input->post('pessoaId');

            $userInfo = array('deletado'=>1,'dataDeletou'=>date('Y-m-d H:i:s'));

            // echo'<pre>'.__FILE__.':'.__LINE__.'<br />';print_r($userInfo);echo'</pre>';die();
            
            
            $result = $this->pessoa_model->deletarPessoa($userId, $userInfo);
            
            if ($result > 0) { 
                echo(json_encode(array('status'=>TRUE))); 
            } else { 
                echo(json_encode(array('status'=>FALSE))); 
            }
        }
        
            
        }

        function informarPresenca() {
  
            if($this->isAdmin() == TRUE) {
                echo(json_encode(array('status'=>'access')));
            } else {
                // echo $userId = $this->input-post('pessoaId');
               // echo $userId = $this->input-post('pessoaId');

        
                $pessoaId = $this->input->post('pessoaId');
                $dia = $this->input->post('dia');
                $presenca = $this->input->post('presenca');

                $userInfo = array('dia'.$dia=>$presenca);
                //echo'<pre>'.__FILE__.':'.__LINE__.'<br />';print_r($pessoaId);echo'</pre>';die();

                $result = $this->pessoa_model->informarPresenca($pessoaId, $dia, $presenca);

                if ($result > 0) { 
                    echo(json_encode(array('status'=>TRUE))); 
                } else { 
                    echo(json_encode(array('status'=>FALSE))); 
                }
              
         

 
            }
        }

    
}