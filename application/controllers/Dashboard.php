<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Dashboard extends BaseController{

    public function __construct() {

        parent::__construct();
        $this->load->model('dashboard_model');
        $this->isLoggedIn(); 

    }

    public function index() {
        
        $this->global['pageTitle'] = 'Dashboard : Resumo de Dados';
        $this->load->model('dashboard_model');
        $this->loadViews("dashboard", $this->global, NULL , NULL);
    }

    public function getResumoPessoas() {


        $this->load->model('dashboard_model');

        $data['resumoPessoas'] = $this->dashboard_model->getResumoPessoas();

        $this->global['pageTitle'] = 'Dashboard : Resumo';

        $this->loadViews("dashboard", $this->global, $data, NULL);

    }

    
    
}