<div class="content-wrapper">    
    <section class="content-header">
      <h1>
        Acesso negado
        <small>Você não está autorizado à este conteúdo</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-center">
                <img src="<?php echo base_url() ?>assets/images/access.png" alt="Acesso negado" />
            </div>
        </div>
    </section>
</div>