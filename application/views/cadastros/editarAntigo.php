<?php

$pessoaId = '';
$nome = '';
$cpf = '';
$nascimento = '';
$sexoId = '';


if(!empty($usuariosGravados))
{
    foreach ($usuariosGravados as $uf)
    {
        $pessoaId = $uf->pessoaId;
        $nome = $uf->nome;
        $cpf = $uf->cpf;
        $nascimento = $uf->nascimento;
        $sexoId = $uf->sexoId;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Administração de Cadastros  
        <small>Editar / Editar Pessoa</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Insira os detalhes do usuário</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="addUser" action="<?php echo base_url() ?>cadastros/editarPessoa" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Nome Completo</label>
                                        <input type="text" class="form-control required" id="nome" value="<?php echo $nome; ?>" name="nome" maxlength="100">
                                        <input type="hidden" value="<?php echo $pessoaId; ?>" name="pessoaId" id="pessoaId" />
                                    </div>                                    
                                </div>

                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">CPF</label>
                                        <input type="text" value="<?php echo $cpf; ?>" class="form-control required" id="cpf" name="cpf" maxlength="14">
                                    </div>                                    
                                </div>
                            </div>
                            
                            <div class="row">
                              
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role">Função</label>
                                        <input type="date" value="<?php echo $nascimento; ?>" class="form-control required" id="nascimento" name="nascimento">
                                    </div>
                                </div>    

                                <div class="col-md-6">
                                
                                    <div class="form-group">
                                        <label for="sexo">Acesso</label>
                                        <select class="form-control required" id="sexoId" name="sexoId">
                                            <?php
                                            if(!empty($sexo))
                                            {
                                                foreach ($sexo as $k => $v)
                                                {
                                                    ?>
                                                    <option value="<?php echo $v->sexoId; ?>" <?php if($v->sexoId == $sexoId) {echo "selected=selected";} ?>><?php echo $v->descricao ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div> <!-- FECHAMENTO DA LINHA-->
                            </div>

                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-success" value="Salvar" />
                            <a href="<?php echo base_url()?>cadastros/listaPessoa" class="btn btn-warning">  Voltar</a>
                            <input type="reset" class="btn btn-default" value="Limpar" />                            
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.min.js" type="text/javascript"></script>

<script type="text/javascript">

    $('#cpf').mask('000.000.000-00', {reverse: true});

</script>