<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Administração de Cadastros
        <small>Adicionar / Editar </small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Insira os detalhes do cadastro</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="adicionarPessoa" action="<?php echo base_url() ?>cadastros/adicionarNovaPessoa" method="post" role="form">
                        <div class="box-body">
                            <div class="row">

                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="nome">Nome Completo</label>
                                        <input type="text" class="form-control required" id="nome" name="nome" maxlength="100">
                                    </div>                                    
                                </div>

                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="v">CPF</label>
                                        <input type="text" class="form-control required" id="cpf" name="cpf" maxlength="14">
                                    </div>                                    
                                </div>

                            </div>
                            
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nascimento">Nascimento</label>
                                        <input type="date" class="form-control required" id="nascimento" name="nascimento">
                                    </div>
                                </div> 

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role">Sexo</label>
                                        <select class="form-control required" id="sexoId" name="sexoId">
                                            <option value="">Selecione</option>
                                            <?php
                                            if(!empty($sexo)) {
                                                foreach ($sexo as $rl) {
                                                    ?>
                                                    <option value="<?php echo $rl->sexoId ?>"><?php echo $rl->descricao ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div> 

                            </div> <!-- FECHAMENTO DA LINHA-->

               
                        </div><!-- /.box-body -->                    
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-success" value="Salvar" />
                            <a href="<?php echo base_url()?>cadastros/listaPessoa" class="btn btn-warning">Voltar</a>
                            <input type="reset" class="btn btn-default" value="Reset" />
                            <!-- <input class="btn btn-success" type=button value="Capturar Foto" onClick="take_snapshot()"> -->
                            
                        </div>
                    </form>

                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/adicionarPessoa.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.min.js" type="text/javascript"></script>

<script type="text/javascript">

    $('#cpf').mask('000.000.000-00', {reverse: true});

</script>

