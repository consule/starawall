<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Cadastros Gerais
        <small>Adicionar, Editar, Apagar</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>cadastros/adicionarNova"><i class="fa fa-plus"></i> Novo</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Lista de Pessoas</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>cadastros/listaPessoa" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Pesquisa"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>Id</th>
                      <th>Nome</th>
                      <th>CPF</th>
                      <th>Criado</th>
                      <th>Sexo</th>
                      <th class="text-center">Ações</th>
                    </tr>
                    <?php
                    if(!empty($usuariosGravados))
                    {
                        foreach($usuariosGravados as $usuario)
                        {
                    ?>
                    <tr>
                      <td><?php echo $usuario->pessoaId ?></td>
                      <td><?php echo $usuario->nome ?></td>
                      <td><?php echo $usuario->cpf ?></td>
                      <td><?php echo date('d/m/Y H:i:s', strtotime($usuario->criado)) ?></td>
                      <td><?php echo $usuario->sexo ?></td>
                      <td class="text-center">
                          <a class="btn btn-sm btn-info" href="<?php echo base_url().'cadastros/editarAntigo/'.$usuario->pessoaId; ?>"><i class="fa fa-pencil"></i></a>
                          <a class="btn btn-sm btn-danger deletarPessoa" href="#" data-userid="<?php echo $usuario->pessoaId; ?>"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/cadastros/deletarPessoa.js" charset="utf-8"></script>
<script type="text/javascript">

    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "listaPessoa/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>

 