<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard
        <small>Painel de Controle</small>
      </h1>
    </section>


    
    <section class="content">
        <div class="row">

       <?php 
       if(!empty($resumoPessoas)) {
         foreach ($resumoPessoas as $k => $v) {
           switch($k) {
             case '0': 
                $cor = 'bg-aqua';
                $icone = 'ion-bag';
              break;
            case '1': 
                $cor = 'bg-green';
                $icone = 'ion-stats-bars';
            break;
            case '2': 
                $cor = 'bg-yellow';
                $icone = 'ion-person-add';
            break;
            case '3': 
                $cor = 'bg-red';
                $icone = 'ion-pie-graph';
            break;
           }
        ?>
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box <?php echo $cor?>">
                <div class="inner">
                  <h3><?php echo $v->total?></h3>
                  <p><?php echo $v->descricao?> </p>
                </div>
                <div class="icon">
                  <i class="ion <?php echo $icone?>"></i>
                </div>
                <a href="<?php echo base_url()?>listaPessoa" class="small-box-footer">Mais Informações  <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <?php }
            }
        ?>


          </div>
    </section>
</div>