<?php

$pessoaId = '';
$nome = '';
$cpf = '';
$funcaoId = '';
$foto = '';
$acessoId = '';



if(!empty($userInfo))
{
    foreach ($userInfo as $uf)
    {
        $pessoaId = $uf->pessoaId;
        $nome = $uf->nome;
        $cpf = $uf->cpf;
        $funcaoId = $uf->funcaoId;
        $foto = $uf->foto;
        $acessoId = $uf->acessoId;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Administração de Pessoas
        <small>Editar / Editar Pessoa</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Insira os detalhes do usuário</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="addUser" action="<?php echo base_url() ?>editarPessoa" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Nome Completo</label>
                                        <input type="text" class="form-control required" id="nome" value="<?php echo $nome; ?>" name="nome" maxlength="100">
                                        <input type="hidden" value="<?php echo $pessoaId; ?>" name="pessoaId" id="pessoaId" />
                                    </div>                                    
                                </div>

                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">CPF</label>
                                        <input type="text" value="<?php echo $cpf; ?>" class="form-control required" id="cpf" name="cpf" maxlength="14">
                                    </div>                                    
                                </div>

                                </div>
                            
                            <div class="row">
                              
                                <div class="col-md-6">
                                
                                    <div class="form-group">
                                        <label for="role">Função</label>
                                        <select class="form-control required" id="role" name="role">
                                            <option value="0">Selecione</option>
                                            <?php
                                            if(!empty($roles))
                                            {
                                                foreach ($roles as $rl)
                                                {
                                                    ?>
                                                    <!-- <option value="<?php echo $rl->funcaoId ?>"><?php echo $rl->descricao ?></option> -->
                                                    <option value="<?php echo $rl->funcaoId; ?>" <?php if($rl->funcaoId == $funcaoId) {echo "selected=selected";} ?>><?php echo $rl->descricao ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>    

                                <div class="col-md-6">
                                
                                    <div class="form-group">
                                        <label for="role">Acesso</label>
                                        <select class="form-control required" id="acesso" name="acesso">
                                            <option value="0">Selecione</option>
                                            <?php
                                            if(!empty($acessos))
                                            {
                                                foreach ($acessos as $rl)
                                                {
                                                    ?>
                                                    <!-- <option value="<?php echo $rl->acessoId ?>"><?php echo $rl->descricao ?></option> -->
                                                    <option value="<?php echo $rl->acessoId; ?>" <?php if($rl->acessoId == $acessoId) {echo "selected=selected";} ?>><?php echo $rl->descricao ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div> 


                                </div> <!-- FECHAMENTO DA LINHA-->

                        <div class="row">
                        <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="role">Webcam</label>
                                   
	
                                        
                                        <div id="my_camera"></div>
                                        
                                        <!-- First, include the Webcam.js JavaScript Library -->
                                        <script type="text/javascript" src="../assets/js/webcam.min.js"></script>
                                        
                                        <!-- Configure a few settings and attach camera -->
                                        <script language="JavaScript">
                                             var cameras = new Array();
                                            navigator.mediaDevices.enumerateDevices().then(function(devices) {
                                                devices.forEach(function(device) {
                                                    var i = 0;
                                                    if(device.kind=== "videoinput"){ //filter video devices only
                                                        cameras[i]= device.deviceId; // save the camera id's in the camera array
                                                        i++;
                                                    }
                                                });
                                            })
                                            Webcam.set({
                                                width: 320,
                                                height: 250,
                                                image_format: 'jpeg',
                                                jpeg_quality: 200, 
                                                constraints: {
                                                    width: 1920,
                                                    height: 1080,
                                                    sourceId: cameras[0]
                                                }
                                            });
                                            Webcam.attach( '#my_camera' );
                                        </script>
                                        
                                        <!-- A button for taking snaps -->
                                        <!-- <form>
                                            <input type=button value="Take Snapshot" onClick="take_snapshot()">
                                        </form> -->
                                        
                                        <!-- Code to handle taking the snapshot and displaying it locally -->
                                        <script language="JavaScript">
                                            function take_snapshot() {
                                                // take snapshot and get image data
                                                Webcam.snap( function(data_uri) {
                                                    // display results in page

                                                    var foto = document.getElementById("results").value = data_uri;

                                                    var element = document.createElement('input');
                                                    element.type = 'hidden';
                                                    element.value = foto;
                                                    element.name= "foto";
                                        

                                                   

                                                    var addUser = document.getElementById('addUser');
                                                    addUser.appendChild(element);    

                                                    console.log(element);

                                                    document.getElementById('results').innerHTML = 
                                                         
                                                        '<img  src="'+data_uri+'"/>';
                                                } );
                                            }
                                        </script>
                                    </div>
                                </div>
                                <label for="role">&nbsp; &nbsp;  Imagem Capturada</label>
                                <div class="col-md-6" id="results">
                                    <div class="form-group" >
                                    <img class="img-responsive" width="300px;" src="<?php echo str_replace('[removed]', 'data:image/png;base64,', $foto) ?>" alt="">
                                    <input type="hidden" name="foto" value="<?php echo str_replace('[removed]', 'data:image/png;base64,', $foto) ?>">
                                    </div>
                                </div>

                            </div>

                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <!-- <input type="reset" class="btn btn-default" value="Reset" /> -->
                            <!-- <input class="btn btn-success" type=button value="Capturar Foto" onClick="take_snapshot()"> -->
                            
                            <a href="<?php echo base_url()?>listaPessoa" class="btn btn-warning">  Voltar</a>
                            <button type="button" class="btn btn-success pull-right"  onClick="take_snapshot()"><i class="fa fa-camera" aria-hidden="true"></i> Capturar Foto</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.min.js" type="text/javascript"></script>

<script type="text/javascript">

    $('#cpf').mask('000.000.000-00', {reverse: true});

</script>