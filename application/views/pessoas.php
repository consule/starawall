<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Manutenção de Pessoas OLD
        <small>Adicionar, Editar, Apagar</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>adicionarNova"><i class="fa fa-plus"></i> Novo</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Lista de Pessoas</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>listaPessoa" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Pesquisa"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>Id</th>
                      <th>Nome</th>
                      <th>CPF</th>
                      <th>Criado</th>
                      <th class="text-center">Foto</th>
                      <th>Função | Acesso</th>
                      <th class="text-center">Ações</th>
                    </tr>
                    <?php
                    if(!empty($userRecords))
                    {
                        foreach($userRecords as $record)
                        {
                    ?>
                    <tr>
                      <td><?php echo $record->pessoaId ?></td>
                      <td><?php echo $record->nome ?></td>
                      <td><?php echo $record->cpf ?></td>
                      <td><?php echo date('d/m/Y H:i:s', strtotime($record->criado)) ?></td>
                      <td> <img class="img-responsive" width="100px;" src="<?php echo str_replace('[removed]', 'data:image/png;base64,', $record->foto) ?>" alt=""> </td>
                      <td>Função: <strong class="text-success"><?php echo $record->descricao .'</strong><br> Acesso: <strong class="text-danger">'.$record->acesso ?></strong></td>
                      <td class="text-center">
                          <a class="btn btn-sm btn-info" href="<?php echo base_url().'editarAntigo/'.$record->pessoaId; ?>"><i class="fa fa-pencil"></i></a>
                          <a class="btn btn-sm btn-danger deleteUser2" href="#" data-userid="<?php echo $record->pessoaId; ?>"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/deleteUser2.js" charset="utf-8"></script>
<script type="text/javascript">

    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "listaPessoa/" + value);
            jQuery("#searchList").submit();
        });
    });

    $(".codigoInscricao").on('click', function(){

      var presenca = $(this).attr('id');
      if(presenca == 1) {
        presenca = 0;
      } else if(presenca == 0) {
        presenca = 1;
      }
     



      var dadosPessoa = $(this).val().split('|');
      var dia = dadosPessoa[0];
      var pessoaId = dadosPessoa[1];
      url = baseURL + 'informarPresenca';

      
     
      $.ajax({
      url: url,
      type : "POST", 
			dataType : "json",
        data: {

          pessoaId: pessoaId,  
          dia: dia,  
          presenca: presenca
        }
        }).done(function(msg){
          location.reload(); 
        });
      });
</script>

 
<style>

.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #5cb85c;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>