<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/



$route['default_controller'] = "login";
$route['404_override'] = 'error';


/*********************************************************************************************** */
/*********************************************************************************************** */
/*********************************************************************************************** */
/*********************************************************************************************** */

//*********************************** LISTAGEM DE PESSOAS ************************************** */



//     'caminho  /view'           'caminho  /controllereClasse/metodo';
$route['cadastros/listaPessoa'] = 'cadastros/pessoa/listaPessoa';
$route['cadastros/listaPessoa/(:num)'] = "cadastros/pessoa/listaPessoa/$1";

//*********************************** ADICIONAR PESSOA ****************************** */

$route['cadastros/adicionarNova'] = "cadastros/pessoa/adicionarNova";
$route['cadastros/adicionarNovaPessoa'] = "cadastros/pessoa/adicionarNovaPessoa";

//*************************** EDITAR PESSOA e USUÁRIOS ******************************* */

$route['cadastros/editarAntigo'] = "cadastros/pessoa/editarAntigo";
$route['cadastros/editarAntigo/(:num)'] = "cadastros/pessoa/editarAntigo/$1";
$route['cadastros/editarPessoa'] = "cadastros/pessoa/editarPessoa";


$route['cadastros/deletarPessoa'] = "cadastros/pessoa/deletarPessoa";



/*********************************************************************************************** */
/*********************************************************************************************** */
/*********************************************************************************************** */
/*********************************************************************************************** */




/*********** USER DEFINED ROUTES *******************/

$route['loginMe'] = 'login/loginMe';
$route['dashboard'] = 'user';

//*************************** LISTAGEM DE PESSOAS **************************** */

$route['listaPessoa'] = 'pessoa/listaPessoa';
$route['listaPessoa/(:num)'] = "pessoa/listaPessoa/$1";

$route['userListing'] = 'user/userListing'; // ok
$route['userListing/(:num)'] = "user/userListing/$1"; // ok

//*************************** ADICIONAR PESSOA e USUÁRIOS **************************** */

$route['adicionarNova'] = "pessoa/adicionarNova";
$route['adicionarNovaPessoa'] = "pessoa/adicionarNovaPessoa";


$route['addNew'] = "user/addNew";
$route['addNewUser'] = "user/addNewUser";

//*************************** EDITAR PESSOA e USUÁRIOS ******************************* */

$route['editarAntigo'] = "pessoa/editarAntigo";
$route['editarAntigo/(:num)'] = "pessoa/editarAntigo/$1";
$route['editarPessoa'] = "pessoa/editarPessoa";

$route['editOld'] = "user/editOld";
$route['editOld/(:num)'] = "user/editOld/$1";
$route['editUser'] = "user/editUser";

//************************** DELETAR PESSOA e USUÁRIOS ******************************* */

$route['deleteUser2'] = "pessoa/deleteUser2";

$route['deleteUser'] = "user/deleteUser";


//************************************************************************************* */

//******************************INFORMAR PRESENÇA ************************************* */

$route['informarPresenca'] = "pessoa/informarPresenca";


//******************************DASHBOARD ******* ************************************* */
$route['dashboard'] = 'dashboard/getResumoPessoas';








$route['loadChangePass'] = "user/loadChangePass";
$route['changePassword'] = "user/changePassword";
$route['pageNotFound'] = "user/pageNotFound";
$route['checkEmailExists'] = "user/checkEmailExists";

$route['forgotPassword'] = "login/forgotPassword";
$route['resetPasswordUser'] = "login/resetPasswordUser";
$route['resetPasswordConfirmUser'] = "login/resetPasswordConfirmUser";
$route['resetPasswordConfirmUser/(:any)'] = "login/resetPasswordConfirmUser/$1";
$route['resetPasswordConfirmUser/(:any)/(:any)'] = "login/resetPasswordConfirmUser/$1/$2";
$route['createPasswordUser'] = "login/createPasswordUser";

/* End of file routes.php */
/* Location: ./application/config/routes.php */

$route['logout'] = 'user/logout';