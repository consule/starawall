<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model{

    function getResumoPessoas() {

        $this->db->select('COUNT(f.`funcaoId`) AS total, f.`descricao`');
        $this->db->from('pessoa p');
        $this->db->join('funcaoPessoa f', 'p.`funcaoId` = f.`funcaoId`');
        $this->db->limit(4);
        $this->db->where('deletado', '0');
        $this->db->group_by('f.`funcaoId`');
        $this->db->order_by('1 desc');
        $query = $this->db->get();
        
        
        return $query->result();

    }    
}


  