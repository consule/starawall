<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Pessoa_model extends CI_Model{
    function listaPessoa($searchText = '', $page, $segment) {

        $this->db->select('pessoaId, nome, cpf, criado, foto, funcaoPessoa.descricao as descricao, acesso.descricao as acesso' );
        $this->db->from('pessoa');
        $this->db->join('funcaoPessoa', 'funcaoPessoa.funcaoId = pessoa.funcaoId','left');
        $this->db->join('acesso', 'acesso.acessoId = pessoa.acessoId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(pessoaId  LIKE '%".$searchText."%'
                            OR  nome  LIKE '%".$searchText."%'
                            OR  funcaoPessoa.descricao  LIKE '%".$searchText."%'
                            OR   acesso.descricao  LIKE '%".$searchText."%'
                            OR  cpf  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('deletado', 0);
        //$this->db->where('BaseTbl.roleId !=', 1);
        $this->db->order_by('pessoaId');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
     
    }

    function getPessoaRoles()
    {
        $this->db->select('funcaoId, descricao');
        $this->db->from('funcaoPessoa');
        $this->db->order_by('descricao');
        $query = $this->db->get();
        
        return $query->result();
    }

    function getPessoaAcesso() {
        $this->db->select('acessoId, descricao');
        $this->db->from('acesso');
        $this->db->order_by('descricao');
        $query = $this->db->get();
        
        return $query->result();
    }

    function contaPessoas($searchText = '') {
        $this->db->select('pessoaId, nome, cpf');
        $this->db->from('pessoa');
        //$this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(pessoaId  LIKE '%".$searchText."%'
                            OR  nome  LIKE '%".$searchText."%'
                            OR  cpf LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('deletado', 0);
        //$this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();
        
        return count($query->result());
    }

    function adicionarNovaPessoa($userInfo) {

        $this->db->trans_start();

        $this->db->insert('pessoa', $userInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;

    }

    function editarPessoa($userInfo, $pessoaId) {

        $this->db->where('pessoaId', $pessoaId);
        $this->db->update('pessoa', $userInfo);

        
        return TRUE;

    }

    function getUserInfo($pessoaId) {
        $this->db->select('pessoaId, nome, cpf, funcaoId, foto, acessoId');
        $this->db->from('pessoa');
        //$this->db->where('deletado', 0);
        $this->db->where('pessoaId', $pessoaId);
        $query = $this->db->get();
    
        return $query->result();
    }

    function deleteUser($userId, $userInfo)
    {
        //echo'<pre>'.__FILE__.':'.__LINE__.'<br />';print_r($userInfo);echo'</pre>';die();
        $this->db->where('pessoaId', $userId);
        $this->db->update('pessoa', $userInfo);
        
        return $this->db->affected_rows();
    }

    function informarPresenca($pessoaId, $dia, $presenca) {

        $this->db->set('dia'.$dia, $presenca);
        $this->db->where('pessoaId', $pessoaId);
        $this->db->update('pessoa'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
        
        return TRUE;
    }





    
    
}


  